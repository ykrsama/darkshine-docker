docker_tag=ykrsama/darkshine-simulation:latest

mkdir -p run
docker run -it --net=host --rm -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
    -v $PWD:$PWD -w $PWD \
    ${docker_tag} "$@"
