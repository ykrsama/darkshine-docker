docker_tag="ykrsama/darkshine-simulation:$(cat tag).$(uname -m)"

DOCKER_BUILDKIT=1 docker buildx build \
    --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
    --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
    --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
    --load \
    --tag ${docker_tag} \
    --cache-to ${docker_tag} \
    --push \
    . 
