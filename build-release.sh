DOCKER_BUILDKIT=1 docker buildx build \
    --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
    --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
    --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
    --push \
    --platform linux/amd64,linux/arm64 \
    --cache-to ykrsama/darkshine-simulation:latest \
    --tag ykrsama/darkshine-simulation:$(cat tag) \
    --tag ykrsama/darkshine-simulation:latest .
