docker_tag=ykrsama/darkshine-simulation:latest

xhost +localhost
echo "Starting ${docker_tag}"

docker run -ti --net=host --rm -e DISPLAY=host.docker.internal:0 \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
    -v $PWD:$PWD -w $PWD \
    ${docker_tag} "$@"
