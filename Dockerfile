# syntax=docker/dockerfile:1
FROM ubuntu:20.04 AS packages
        LABEL maintainer.name="Dark SHINE R&D team"
        LABEL version="20240220"
        LABEL maintainer.email="xuliangz@sjtu.edu.cn"
        
        SHELL ["/bin/bash", "-c"]
        
        ENV LANG=C.UTF-8
        
        # install general dependencies
        COPY packages packages
        
        ARG DEBIAN_FRONTEND=noninteractive
        #ARG UBUNTU_MIRROR=http://mirrors.ustc.edu.cn/
        #ARG UBUNTU_MIRROR=http://mirrors.aliyun.com/
        ARG UBUNTU_MIRROR=http://mirror.sjtu.edu.cn/
        
        RUN sed -i "s#http://archive.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            sed -i "s#http://ports.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            sed -i "s#http://security.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            apt-get update &&\
            ln -sf /usr/share/zoneinfo/UTC /etc/localtime &&\
            apt-get -y install curl wget git vim gdb valgrind linux-tools-generic $(cat packages) &&\
            dpkg-reconfigure locales &&\
            apt-get autoremove -y &&\
            apt-get clean &&\
            rm -rf /var/lib/apt/lists/* &&\
            rm /usr/bin/perf &&\
            ln -s /usr/lib/linux-tools/*-generic/perf /usr/bin/perf &&\
            strip --remove-section=.note.ABI-tag /usr/lib/*-linux-gnu/libQt5Core.so &&\
            ldconfig
            # replace perf to fix the error:"perf not found for kernel xxx"
            # strip is to solve the problem in qt5 which checks the compatibility with the host kernel.
        RUN pip install --no-cache-dir --upgrade pip && \
            pip install --no-cache-dir pybind11


# Build BOOST
FROM packages AS boost
        ARG BOOST_VERSION=1_78_0
        
        WORKDIR /root
                ADD Downloads/boost_${BOOST_VERSION}.tar.gz .
                
                RUN cd boost_${BOOST_VERSION} &&\
                    ./bootstrap.sh --prefix=/opt/boost &&\
                    ./b2 -j$(nproc) install

# General base
FROM packages AS base
        ENV BOOST_ROOT="/opt/boost"
        ENV BOOST_LIBRARYDIR="${BOOST_ROOT}/lib"
        ENV BOOST_INCLUDEDIR="${BOOST_ROOT}/include"
        ENV LD_LIBRARY_PATH="${BOOST_ROOT}/lib:${LD_LIBRARY_PATH}"
        ENV BOOSTROOT="${BOOST_ROOT}"
        ENV BOOST_LIB="${BOOST_ROOT}/lib"
        ENV BOOST_IGNORE_SYSTEM_PATHS=1
        
        COPY --from=boost ${BOOST_ROOT} ${BOOST_ROOT}

# Build ROOT
FROM base AS root
        ARG ROOT_VERSION=6.26.14
        
        WORKDIR /root
                ADD Downloads/root_v${ROOT_VERSION}.source.tar.gz .
                
                RUN mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=/opt/root \
                          -DCMAKE_CXX_STANDARD=17 \
                          -DCXX_STANDARD_STRING=17 \
                          ../root-${ROOT_VERSION} ;\
                    cmake --build . --target install -- -j$(nproc)

# Build Geant4
FROM base AS geant4
        ARG G4_VERSION=10.6.3
        
        WORKDIR /root
                ADD Downloads/geant4-v${G4_VERSION}.tar.gz .
                
                RUN mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=/opt/geant4 \
                          -DGEANT4_INSTALL_DATA=OFF \
                          -DGEANT4_USE_GDML=ON \
                          -DGEANT4_USE_QT=ON \
                          -DGEANT4_USE_PYTHON=ON \
                          -DGEANT4_BUILD_CXXSTD=17 \
                          -DGEANT4_USE_OPENGL_X11=OFF \
                          ../geant4-v${G4_VERSION} &&\
                    make -j$(nproc) install
                
                COPY Downloads/geant4-data-v${G4_VERSION} /opt/geant4/share/Geant4-${G4_VERSION}/data

# Build ACTS
FROM base AS acts
        # From branch xuliang-v30
        ARG ACTS_BRANCH="xuliang-v30"
        ARG ACTS_GIT_URL="https://github.com/ykrsama/acts.git"
        #ARG ACTS_GIT_URL="https://github.com/acts-project/acts.git"

        WORKDIR /root
                COPY --from=root /opt/root /opt/root
                COPY --from=geant4 /opt/geant4 /opt/geant4
                
                RUN source /opt/root/bin/thisroot.sh &&\
                    source /opt/geant4/bin/geant4.sh &&\
                    git clone --branch ${ACTS_BRANCH} --depth=1 ${ACTS_GIT_URL} &&\
                    cd acts &&\
                    git checkout 88a3dd1080412e087974153404f72a76b41e27e9 &&\
                    cd .. &&\
                    mkdir -p build &&\
                    cd build &&\
                    cmake -Wno-dev \
                          -DCMAKE_CXX_STANDARD=17 \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
                          -DCMAKE_INSTALL_PREFIX=/opt/acts \
                          -DACTS_BUILD_FATRAS=OFF \
                          -DACTS_BUILD_PLUGIN_TGEO=ON \
                          -DACTS_BUILD_PLUGIN_JSON=ON \
                          -DACTS_BUILD_EXAMPLES=ON \
                          -DACTS_BUILD_EXAMPLES_BINARIES=ON \
                          -DACTS_BUILD_EXAMPLES_PYTHON_BINDINGS=ON \
                          -DACTS_USE_SYSTEM_PYBIND11=ON \
                          -Dpybind11_DIR=$(pybind11-config --cmakedir) \
                          ../acts &&\
                    make -j$(nproc) install
                RUN cp /root/build/compile_commands.json /root/acts

# Build DSS
FROM base AS dss
        ARG DSS_BRANCH=master
        ARG DSS_DIR=/opt/darkshine-simulation/install

        COPY --from=root /opt/root /opt/root
        COPY --from=geant4 /opt/geant4 /opt/geant4
        COPY --from=acts /opt/acts /opt/acts

        ARG ACTS_DIR="/opt/acts"
        ENV PATH="${ACTS_DIR}/bin:${PATH}"
        ENV LD_LIBRARY_PATH="${ACTS_DIR}/lib:${LD_LIBRARY_PATH}"
      
        WORKDIR /opt/darkshine-simulation
                RUN --mount=type=secret,id=id_rsa \
                    --mount=type=secret,id=id_rsa.pub \
                    --mount=type=secret,id=known_hosts \
                    mkdir -p /root/.ssh &&\
                    chmod 700 /root/.ssh &&\
                    cp /run/secrets/id_rsa /root/.ssh &&\
                    cp /run/secrets/id_rsa.pub /root/.ssh &&\
                    cp /run/secrets/known_hosts /root/.ssh &&\
                    source /opt/root/bin/thisroot.sh &&\
                    source /opt/geant4/bin/geant4.sh &&\
                    git clone --branch ${DSS_BRANCH} --depth=1 git@code.ihep.ac.cn:darkshine/darkshine-simulation.git source &&\
                    mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=${DSS_DIR} ../source &&\
                    make -j$(nproc) install &&\
                    cd .. &&\
                    rm source/Tools/scripts/ref.dp_ana.root &&\
                    rm -rf build &&\
                    rm -rf source/.git &&\
                    rm -rf /root/.ssh
        
# Madgraph
FROM base AS mg5
        ARG MG5_VERSION=3_5_4
        ARG HEPMC2_VERSION=2.06.09

        ENV ROOTSYS /opt/root
        ENV PATH ${ROOTSYS}/bin:$PATH
        ENV PYTHONPATH ${ROOTSYS}/lib:${PYTHONPATH}
        ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH}/lib:${LD_LIBRARY_PATH}
        ENV MAKEFLAGS=-j$(nproc)
        ENV MG5_DIR=/opt/MG5_aMC
        ENV HEPMC2_DIR=${MG5_DIR}/HEPTools/hepmc
       
        COPY --from=root /opt/root /opt/root
       
        ADD Downloads/MG5_aMC_v${MG5_VERSION}.tar.gz /opt
        
        RUN mv /opt/MG5_aMC_v${MG5_VERSION} ${MG5_DIR}

        # Manually install hepmc to fix problem in linux/arm64
        WORKDIR /root
                ADD Downloads/hepmc${HEPMC2_VERSION}.tgz .
                
                RUN mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=${HEPMC2_DIR} \
                          -Dmomentum:STRING=GEV \
                          -Dlength:STRING=MM \ 
                          ../hepmc${HEPMC2_VERSION} &&\
                    make install
        COPY root /root
        WORKDIR ${MG5_DIR}
                # Soft Link Boost
                RUN mkdir -p HEPTools/boost &&\
                    mkdir -p HEPTools/boost/boost_ &&\
                    ln -s /opt/boost/lib HEPTools/boost/lib &&\
                    ln -s /opt/boost/include HEPTools/boost/include &&\
                    ln -s /opt/boost/include/boost HEPTools/boost/boost_/boost

                RUN ./bin/mg5_aMC /root/mg5_install_cmd &&\
                    echo 1

FROM base AS vim_ycm
        ENV MAKEFLAGS=-j$(nproc)

        COPY root /root

        RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
            https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

        RUN vim +PlugInstall +qall

        WORKDIR /root/.vim/plugged/YouCompleteMe
                RUN ./install.py
        # Do some clean-up
        WORKDIR /root/.vim/plugged
                RUN rm -rf */.git

# Release
FROM base AS release
        WORKDIR /root
        COPY --from=root /opt/root /opt/root
        COPY --from=geant4 /opt/geant4 /opt/geant4
        COPY --from=acts /opt/acts /opt/acts
        COPY --from=acts /root/acts /root/acts
        COPY --from=dss /opt/darkshine-simulation /opt/darkshine-simulation
        COPY --from=mg5 /opt/MG5_aMC /opt/MG5_aMC
        COPY --from=vim_ycm /root/.vim /root/.vim
        # Copy scripts
        COPY root /root
        COPY entry-point.sh /entry-point.sh
        
        ENTRYPOINT ["/entry-point.sh"]
        
        CMD source /root/dss.sh &&\
            mkdir -p run &&\
            cd run &&\
            if [[ ! -f default.yaml ]]; then cp /opt/darkshine-simulation/source/DP_simu/scripts/default.yaml . ; fi &&\
            if [[ ! -f init_vis.mac ]]; then cp /opt/darkshine-simulation/source/DP_simu/scripts/init_vis.mac . ; fi &&\
            if [[ ! -f gui.mac      ]]; then cp /opt/darkshine-simulation/source/DP_simu/scripts/gui.mac . ; fi &&\
            if [[ ! -f mag_default.root ]]; then cp /opt/darkshine-simulation/source/DP_simu/scripts/mag*.root . ; fi &&\
            if [[ ! -f config.txt   ]]; then DAna -x > config.txt ; fi &&\
            bash
