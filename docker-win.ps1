$HostIP=(Get-NetIPConfiguration | Where-Object {$_.IPv4DefaultGateway -ne $null -and $_.NetAdapter.Status -ne "Disconnected"}).IPv4Address.IPAddress 

docker run -it -e DISPLAY=${HostIP}:0.0 -v ${PWD}:/root/workspace -w /root/workspace ykrsama/darkshine-simulation:latest 
