alias ls='ls --color=auto'
alias ll='ls -la'
alias grep='grep --color=auto'
export PS1='\[\e[32;1m\]\u@\h:\[\e[35;1m\]\w\[\e[0m\]# '
