call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'
Plug 'ycm-core/YouCompleteMe', { 'commit':'d98f896' }
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting

" YCM setup
let g:ycm_global_ycm_extra_conf='~/global_extra_conf.py'
" Enable debugging
let g:ycm_keep_logfiles = 1
let g:ycm_log_level = 'debug'
" If you're on an arm mac, uncomment the following line
"let g:ycm_clangd_binary_path=trim(system('brew --prefix llvm')).'/bin/clangd'
let &rtp .= ',' . expand( '<sfile>:p:h' )
filetype plugin indent on
let g:ycm_goto_buffer_command = 'new-tab'
let g:ycm_clangd_binary_path = '/usr/bin/clangd'
nnoremap gd :YcmCompleter GoToDefinitionElseDeclaration<CR>
nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
nnoremap <leader>jc :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>st :YcmCompleter GetType<CR>
nnoremap <F5> :YcmForceCompileAndDiagnostics<CR>

" vim-airline setups
let g:airline_powerline_fonts=0
let g:airline#extensions#tabline#enabled=1
nnoremap <leader>[ :tabp<CR>
nnoremap <leader>] :tabn<CR>

" copy & paste
noremap <leader>y "*y
" Backspace
set backspace=indent,eol,start
" Highlight
syntax enable
set syntax=on
" 去掉输入错误的提示声音
"set noeb
" 在处理未保存或只读文件的时候，弹出确认
set confirm
" 自动缩进
set autoindent
set cindent
" Tab键的宽度
set tabstop=4
" 统一缩进为4
set softtabstop=4
set shiftwidth=4
" 不要用空格代替制表符
set expandtab
" 在行和段开始处使用制表符
set smarttab
" 显示行号
" set number
" 历史记录数
set history=1000
"禁止生成临时文件
set nobackup
set noswapfile
"搜索忽略大小写
set ignorecase
set smartcase
"搜索逐字符高亮
set hlsearch
set incsearch
"行内替换
set gdefault
" 命令行（在状态行下）的高度，默认为1，这里是2
set cmdheight=2
" 侦测文件类型
"filetype on
" 高亮显示匹配的括号
set showmatch
" 为C程序提供自动缩进
set smartindent
" 光标移动到buffer的顶部和底部时保持3行距离
set scrolloff=3
" let vim stop handling mouse
"set mouse=a
" enable folding
set foldenable
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
" space open/closes folds
nnoremap <space> za
set foldmethod=indent   " fold based on indent level
