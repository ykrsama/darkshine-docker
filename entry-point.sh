#!/bin/bash
set -e 

source /opt/root/bin/thisroot.sh
source /opt/geant4/bin/geant4.sh

# ACTS
ACTS_DIR="/opt/acts"
export PATH="${ACTS_DIR}/bin:${PATH}"
export LD_LIBRARY_PATH="${ACTS_DIR}/lib:${LD_LIBRARY_PATH}"

# Madgraph
export PATH="/opt/MG5_aMC/bin:${PATH}"

exec "$@"
