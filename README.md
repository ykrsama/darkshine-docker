# DarkSHINE Docker 【暗光刀客】

[Dockerfile](https://gitlab.com/ykrsama/darkshine-docker/-/blob/master/Dockerfile) | [Release](https://hub.docker.com/r/ykrsama/darkshine-simulation)

![Dockerfile Graph](https://gitlab.com/ykrsama/darkshine-docker/-/raw/master/Dockerfile.pdf)

## Get startup scripts

In a terminal or PowerShell:

```bash
git clone https://gitlab.com/ykrsama/darkshine-docker.git
```

Which contains scripts for **Docker** on MacOS, Linux, WSL, Windows, and **Singularity** on Linux. Currently, GUI is only supported on **local** hosts.

[[_TOC_]]

## MacOS

1. **[Install Docker](https://www.docker.com/get-started/)**

2. **Setup X11 forwarding for Event Display Apps**

   1. Install [XQuartz](https://www.xquartz.org)

   2. To enable the OpenGL rendering, run in the host terminal:
   ```bash
   defaults write org.xquartz.X11 enable_iglx -bool true
   ```
   
   3. From the XQuartz preferences, in the security tab, make sure **Allow connections from network clients** is enabled.

   4. Restart XQuartz.

3. **Run Simple example**

   ```bash
   cd darkshine-docker
   ./docker-macos.sh # Mount current directory, mkdir run, and create config files
   # Test
   DSimu -b 100 # Simulation, reads default.yaml, outputs dp_simu.root
   DDis # Event display, reads dp_simu.root
   exit
   ```

4. **Alias command**

   ```bash
   echo alias dss-docker=\"$PWD/docker-macos.sh bash --init-file /root/dss.sh\" >> ~/.zshrc
   source ~/.zshrc
   ```

   ```dss-docker```  will simply mount the current directory, without creating config files.

## Linux / WSL Docker

1. **Install Docker and run**

   ```bash
   sudo apt install docker.io # Threre are some problem in snap version
   cd darkshine-docker
   ./docker-linux.sh # Mount current directory, mkdir run, and create config files
   # Test
   DSimu -b 100 # Simulation, reads default.yaml, outputs dp_simu.root
   DDis # Event display, reads dp_simu.root
   exit
   ```

2. **Alias command**

   ```bash
   echo alias dss-docker=\"$PWD/docker-linux.sh bash --init-file /root/dss.sh\" >> ~/.bashrc
   source ~/.bashrc
   ```

   ```dss-docker```  will simply mount the current directory, without creating config files.

## Windows Docker

1. **[Install Docker](https://www.docker.com/get-started/)**

2. **Setup X11 forwarding**

   1. Run a new PowerShell as an **Administrator**:

      ```
      choco install vcxsrv
      ```

   2. Run Xlaunch from start menu, go with all the default settings, however do check **Disable access control**. X server should show up in your tray icons.

3. **Run (PowerShell as normal user)**

   ``` bash
   cd darkshine-docker
   ./docker-win.ps1 # Mount current directory, mkdir run, and create config files
   # Test
   DSimu -b 100 # Simulation, reads default.yaml, outputs dp_simu.root
   DDis # Event display, reads dp_simu.root
   exit
   ```

## Singularity - Linux

1. Install singularity. If you have access to a machine running [CVMFS](https://cernvm.cern.ch/fs/), simply

   ```bash
   export PATH="/cvmfs/atlas.cern.ch/repo/containers/sw/singularity/x86_64-el7/current/bin:$PATH"
   ```

2. For SJTU bl-0 cluster: don't work in the login node

   ```bash
   condor_submit -i
   ```

3. Run

   ```bash
   cd darkshine-docker
   ./singularity.sh
   echo alias dss-docker=\"$PWD/singularity.sh bash --init-file /root/dss.sh\" >> ~/.bashrc
   source ~/.bashrc
   ```

## For Developers

The Docker supports debugger ```gdb```, dynamic analysis tools ```valgrind```, and profiler ```perf```.

### CLion example settings

**Toolchains**

- Name: ```DSS```
- Image: ```ykrsama/darkshine-simulation:latest```
- Container Settings: ```-e DISPLAY=host.docker.internal:0 -v /tmp/.X11-unix:/tmp/.X11-unix:ro --net host --rm```

**CMake**

- Toolchain: ```DSS```
- Uncheck: Environment - **Include system environment variables**

**Run/Debug Configurations**

- Environment variables:

  ```
  HOME /root
  DISPLAY host.docker.internal:0
  ```

- Environment variables - **Load variables from file**: ```/entry-point.sh```


# Note

docker using proxy

1. /etc/docker/daemon.json

```
{
  "proxies": {
    "http-proxy": "http://172.22.32.1:7890",
    "https-proxy": "http://172.22.32.1:7890",
    "no-proxy": "127.0.0.0/8"
  }
}
```

2. set proxy for buildx builder by creating new builder instance

```
docker buildx create --name mybuilder --driver docker-container \
--driver-opt env.http_proxy='http://172.22.32.1:7890' \
--driver-opt env.https_proxy='http://172.22.32.1:7890' \
--driver-opt env.no_proxy='127.0.0.1'
```

